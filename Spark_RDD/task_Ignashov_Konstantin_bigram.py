import re
import collections

from pyspark.sql import SparkSession

DEPLOYMENT = "yarn"
if DEPLOYMENT == "yarn":
    spark = SparkSession.builder \
        .master("yarn") \
        .appName("kignashov") \
        .config("spark.driver.memory", "512m") \
        .config("spark.driver.cores", "1") \
        .config("spark.executor.instances", "3") \
        .config("spark.executor.cores", "1") \
        .config("spark.executor.memory", "1g") \
        .getOrCreate()
elif DEPLOYMENT == "local":
    spark = SparkSession.builder \
        .master("local[2]") \
        .appName("spark-course") \
        .config("spark.driver.memory", "512m") \
        .getOrCreate()
else:
    raise NotImplementedError("Deployment {d} is not supported!".format(d=DEPLOYMENT))

sc = spark.sparkContext

rdd = sc.textFile("/data/wiki/en_articles_part")
lower_rdd = rdd.map(lambda c: c.lower())


def get_bigrams(text):
    text_list = text.split('\t')[1]
    text_list = re.findall(r"\w+", text_list)
    bigrams = []
    cur_bigram = []
    for word in text_list:
        if len(cur_bigram) == 1:
            cur_bigram.append(word)
            bigrams.append(cur_bigram[0] + '_' + cur_bigram[1])
            cur_bigram = []
        if word == 'narodnaya':
            cur_bigram = ['narodnaya']
    return bigrams


result_dict = lower_rdd.map(get_bigrams).flatMap(lambda x: x).map(lambda x: (x, 1)).countByKey()
od = collections.OrderedDict(sorted(result_dict.items()))

for key in od:
    print(key, od[key], sep='\t')
