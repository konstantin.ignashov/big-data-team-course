import re
import copy
import math
from operator import itemgetter

from pyspark.sql import SparkSession

DEPLOYMENT = "yarn"
if DEPLOYMENT == "yarn":
    spark = SparkSession.builder \
        .master("yarn") \
        .appName("kignashov") \
        .config("spark.driver.memory", "512m") \
        .config("spark.driver.cores", "1") \
        .config("spark.executor.instances", "3") \
        .config("spark.executor.cores", "1") \
        .config("spark.executor.memory", "1g") \
        .getOrCreate()
elif DEPLOYMENT == "local":
    spark = SparkSession.builder \
        .master("local[2]") \
        .appName("spark-course") \
        .config("spark.driver.memory", "512m") \
        .getOrCreate()
else:
    raise NotImplementedError("Deployment {d} is not supported!".format(d=DEPLOYMENT))

sc = spark.sparkContext


rdd = sc.textFile("/data/wiki/en_articles_part")
lower_rdd = rdd.map(lambda c: c.lower())
rdd_stop_words = sc.textFile('/data/stop_words/stop_words_en-xpo6.txt')

stop_list = list(rdd_stop_words.take(1500))


def get_bigrams(text):
    text_list = text.split('\t')[1]
    text_list = re.findall(r"\w+", text_list)
    bigrams = []
    word_1 = None
    word_2 = None
    for word in list(text_list):
        if word in stop_list:
            continue
        if (word_1 is None) and (word_2 is None):
            word_1 = word
        elif word_2 is None:
            word_2 = word
            bigrams.append(word_1 + '_' + word_2)
        else:
            word_1 = copy.copy(word_2)
            word_2 = word
            bigrams.append(word_1 + '_' + word_2)
    return bigrams


def get_pure_words(text):
    text_list = text.split('\t')[1]
    text_list = re.findall(r"\w+", text_list)
    words = []
    for word in list(text_list):
        if word in stop_list:
            continue
        else:
            words.append(word)
    return words


pure_words = lower_rdd.map(get_pure_words).flatMap(lambda x: x)
pure_words_counts = pure_words.map(lambda x: (x, 1)).reduceByKey(lambda x, y: x + y)

clear_bigrams = lower_rdd.map(get_bigrams).flatMap(lambda x: x)
total_number_of_word_pairs = clear_bigrams.count()
total_number_of_words = total_number_of_word_pairs + 4100
clear_bigrams_counts = clear_bigrams.map(lambda x: (x, 1)).reduceByKey(lambda x, y: x + y)
relevant_clear_bigrams_counts = clear_bigrams_counts.filter(lambda x: x[1] >= 500)
relevant_clear_bigrams_counts_list = relevant_clear_bigrams_counts.take(100)


def npmi(bigram):
    freq_bigram = bigram[1]
    word_1, word_2 = bigram[0].split('_')
    freq_first = pure_words_counts.filter(lambda x: x[0] == word_1).take(10)[0][1]
    freq_second = pure_words_counts.filter(lambda x: x[0] == word_2).take(10)[0][1]
    p_a = freq_first / total_number_of_words
    p_b = freq_second / total_number_of_words
    p_ab = freq_bigram / total_number_of_word_pairs
    pmi = math.log(p_ab / (p_a * p_b))
    npmi_result = pmi / -math.log(p_ab)
    return npmi_result


result = []
for bigram in relevant_clear_bigrams_counts_list:
    result.append((bigram[0], round(npmi(bigram), 3)))

for bigram in sorted(result, key=itemgetter(1), reverse=True)[:39]:
    print(bigram[0], bigram[1], sep='\t')
