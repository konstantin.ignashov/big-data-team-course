import pyspark.sql.functions as F
from pyspark.sql import SparkSession

DEPLOYMENT = "yarn"

if DEPLOYMENT == "yarn":
    spark = SparkSession.builder \
                        .master("yarn") \
                        .appName("kignashov") \
                        .config("spark.driver.memory", "512m") \
                        .config("spark.driver.cores", "1") \
                        .config("spark.executor.instances", "3") \
                        .config("spark.executor.cores", "1") \
                        .config("spark.executor.memory", "1g") \
                        .getOrCreate()
elif DEPLOYMENT == "local":
    spark = SparkSession.builder \
                        .master("local[2]") \
                        .appName("spark-course") \
                        .config("spark.driver.memory", "512m") \
                        .getOrCreate()
else:
    raise NotImplementedError("Deployment {d} is not supported!".format(d=DEPLOYMENT))
    
sc = spark.sparkContext

full_df = spark.read.format("csv").options(delimiter='\t').load("/data/twitter/twitter.txt")
full_df = full_df.withColumn('level', F.lit(None)).withColumn('in_queue', F.lit(0))


def bfs(s, target, df):
    df = df.withColumn("level", F.when(F.col('_c1') == s, 0).otherwise(F.col('level')))

    df = df.withColumn("in_queue", F.when(F.col('_c1') == s, 1).otherwise(F.col('in_queue')))

    cur_level = 0
    while True:
        cur_level += 1
        cur_v = df.filter(F.col('in_queue') == 1).withColumn("in_queue_new", F.lit(1))\
            .withColumn("level_new", F.lit(cur_level))\
            .select(['_c0', "in_queue_new", "level_new"]).withColumnRenamed("_c0", "_c1").distinct()
        df = df.withColumn("in_queue", F.lit(0))

        df = df.join(cur_v, '_c1', 'left')\
            .withColumn("in_queue_final", F.when(df.level.isNull(),
                                                 F.col('in_queue_new')).otherwise(F.col('in_queue')))\
            .withColumn("level_final", F.when(df.level.isNull(), F.col('level_new')).otherwise(F.col('level')))\
            .drop("in_queue_new", "level_new", 'level', 'in_queue')\
            .withColumnRenamed("in_queue_final", "in_queue")\
            .withColumnRenamed("level_final", "level")
        target_count = df.filter(~F.col('level').isNull()).filter(F.col('_c0') == target).count()
        if target_count != 0:
            break
            
    return cur_level + 1


print(bfs(12, 34, full_df))
